package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestSheIn {

    @Test
    public void testSheIn() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.shein.com");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Women's Clothing, Clothes & Fashion, Shop Dresses Online | SHEIN", driver.getTitle());
       
        WebElement shoppingBag = driver.findElement(By.xpath("/html/body/div[1]/header/div[2]/div[1]/div/div[1]/div/div[3]/div[2]/a"));
        shoppingBag.click();

        
        WebElement moveToWishList = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[3]/div[1]/div/table/tbody/tr[2]/td[1]/div[2]/div/div[3]/div[1]/span/span"));
        moveToWishList.click();

        
        WebElement confirmYes = driver.findElement(By.xpath("/html/body/span/div/div/button[2]"));
        confirmYes.click();

        driver.get("https://www.shein.com/cart");
        driver.quit();
    }
}
